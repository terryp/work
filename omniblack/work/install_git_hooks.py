from sys import stderr
from pathlib import Path
from functools import partial
from os import EX_OK, EX_NOINPUT
from os.path import lexists

from .wrk_file import get_file
from .hooks import hooks

printerr = partial(print, file=stderr)


def install_hooks(src_path: Path):
    git_dir = src_path/'.git'
    hooks_dir = git_dir/'hooks'

    if not hooks_dir.exists():
        printerr(f'{src_path} is not a git project.')
        printerr('Cannot install git hooks')
        return EX_NOINPUT

    with get_file('scripts/run_git_hook.sh') as run_git_hook:
        for hook in hooks:
            hook_path = hooks_dir/hook
            try:
                hook_path.symlink_to(run_git_hook)
            except FileExistsError:
                if hook_path.exists():
                    if hook_path.resolve() != run_git_hook:
                        printerr(
                            f'{hook_path} exists'
                            + f' and does not point to {run_git_hook}'
                        )
                else:
                    printerr(f'{hook_path} is a broken symlink.')

    return EX_OK
