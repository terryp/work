#! /usr/bin/python3

from argparse import BooleanOptionalAction, SUPPRESS
from ruamel.std.argparse import ProgramBase, option, sub_parser, SmartFormatter
from git import Repo
from pathlib import Path
from subprocess import run
from os import environ, getcwd, EX_OK, EX_USAGE
from sys import exit, stderr
from functools import partial
from textwrap import dedent

from textual.app import App
from textual.widgets import ScrollView
from rich.table import Table

from .config import config
from .projects import projects
from .fork import fork
from .install_git_hooks import install_hooks

printerr = partial(print, file=stderr)

envs = tuple(
    path.name
    for path in config.path
    if path.is_dir()
)


ignored_args = {'subparser_level_0', 'func'}


usage = """
    mkwrk [-h] [--help-all] <project> [new_environment]
    mkwrk [-h] [--help-all] {list,refresh,clone,fork} ...
"""

description = dedent("""
    Create a new development environment for a project on this computer.
""")


class ProjectList(App):
    def __init__(self, *args, projects, **kwargs):
        super().__init__(*args, **kwargs)
        self.projects = projects

    async def on_load(self) -> None:
        """Sent before going in to application mode."""
        await self.bind("q", "quit", "Quit")

    async def on_mount(self) -> None:
        """Call after terminal goes in to application mode"""

        table = Table(title='Available projects', show_lines=True)
        table.add_column('Name')
        table.add_column('Description')

        for name, project in self.projects.items():
            table.add_row(name, project['desc'])
        # Create our widgets
        # In this a scroll view for the code and a directory tree
        self.body = ScrollView(table)
        await self.view.dock(self.body)


class MkWrk(ProgramBase):
    @option(
        '--help-all',
        help='Show the help for all subcommands, and exit',
        const=True,
        default=SUPPRESS,
        nargs='?',
    )
    def __init__(self):
        super().__init__(
            formatter_class=SmartFormatter,
            usage=usage,
            description=description,
        )

    def run(self):
        args = {
            key: value
            for key, value in vars(self._args).items()
            if key not in ignored_args
        }

        if self._args.func:
            return self._args.func(**args)

    def parse_args(self):
        self._parse_args(help_all=True, default_sub_parser='clone')

    @sub_parser(
        help='List projects that are available to clone.',
        description='List all projects that are available to clone.',
        usage='mkwrk list [-h]',
    )
    @option(
        '--pretty',
        action=BooleanOptionalAction,
        help='Should the output be pretty printed?',
        default=True,
    )
    def list(self, pretty):
        """
        List all projects that mkwrk can make an environment for.
        """
        if not pretty:
            available_projects = '\n\n'.join(
                f'{name}: {project["desc"] or None}'
                for name, project in projects.items()
            )
            print(available_projects)
        else:
            ProjectList.run(projects=projects)

    @sub_parser(
        help='Run post-clone tasks for an environment',
        description='Run post-clone tasks for an environment',
        usage='mkwrk refresh [-h] <env>',
    )
    @option(
        'env',
        help='The environment to refresh.',
        choices=envs,
        metavar='env',
    )
    def refresh(self, env):
        try:
            path = config.path.get(env)
        except FileNotFoundError as err:
            printerr(*err.args)
            return EX_USAGE

        return self.post_clone(path, env)

    def post_clone(self, path: Path, env_name: str):
        new_env = environ | {
            'SRC': str(path),
            'WRK_ENV': env_name,
            'OLD_LOC': getcwd(),
        }

        mkwrk_ext = Path.joinpath(path, 'dev-tools', 'mkwrk.ext')

        if mkwrk_ext.exists():
            run([str(mkwrk_ext)], cwd=path, env=new_env)

        code = install_hooks(path)
        if code != EX_OK:
            return code

        return EX_OK

    def create_env(self, remote_url, env_name):
        path = config.path.write/env_name
        Repo.clone_from(remote_url, path)

        return self.post_clone(path, env_name)

    @sub_parser(
        help='Create a new environment for a project',
        description='Create a new environment for a project',
        usage='mkwrk clone [-h] <project> [new_environment]',
    )
    @option(
        'project',
        choices=projects.keys(),
        metavar='project',
        help='The project to clone.',
    )
    @option(
        'new_environment',
        default=None,
        nargs='?',
        help='The name of the new environment. Defaults to the project name.'
    )
    def clone(self, project, new_environment=None):
        if new_environment is None:
            new_environment = project

        remote_url = projects[project]['url']
        return self.create_env(remote_url, new_environment)

    @sub_parser(
        help='Fork a project and create an environment for it',
        description='Fork a project and create an environment for it',
        usage='mkwrk fork [-h] <fork_url> [new_environment]',
    )
    @option('fork_url', help='The url of the project to fork.')
    @option(
        'new_environment',
        help='The name of the new environment.',
        nargs='?',
        default=None,
    )
    def fork(self, fork_url, new_environment):
        (repo_name, remote_url) = fork(fork_url)

        if new_environment is None:
            new_environment = repo_name

        return self.create_env(remote_url, new_environment)


def main():
    cmd = MkWrk()
    cmd.parse_args()
    exit(cmd.run())


if __name__ == '__main__':
    main()
