import inquirer
from inspect import signature as get_signature, Parameter, Signature
from functools import wraps
from typing import Annotated

pos_args_type = {Parameter.POSITIONAL_ONLY, Parameter.POSITIONAL_OR_KEYWORD}


def create_validator(param_type: type):
    def validator(answers, current):
        try:
            type(current)
        except TypeError:
            return False
        else:
            return True

    return validator


def create_question(param: Parameter):
    param_type: type = param.annotation.__origin__
    message: str = param.annotation.__metadata__[0]
    default = param.default
    args = dict(
        validate=create_validator(param_type),
        message=message,
    )

    if default is not Parameter.empty:
        args['default'] = default

    return inquirer.Text(**args)


def prompt_args(func):
    signature = get_signature(func)
    if __debug__:
        has_untyped = any(
            parameter.type is Parameter.empty
            for name, parameter in signature.parameters
        )

        if has_untyped:
            raise TypeError('All parameters must be annotated')

    pos_args = tuple(
        create_question(parameter)
        for parameter in signature.parameters.values()
        if parameter.type in pos_args_type
    )

    @wraps(func)
    def wrapper(*args, **kwargs):
        bound_args = signature.bind_partial(*args, **kwargs)
        needed_pos_args = pos_args[len(bound_args):]


