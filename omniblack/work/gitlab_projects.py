from functools import wraps
from gitlab import Gitlab
from .config import config


gitlab = None


def needs_gitlab(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        global gitlab

        if gitlab is None:
            keys = config.keys

            if 'Gitlab' not in keys:
                raise KeyError('Gitlab auth token is not in keys file')

            gitlab = Gitlab('https://gitlab.com', private_token=keys.Gitlab)

        return func(*args, **kwargs)

    return wrapper


@needs_gitlab
def projects():
    for project in gitlab.projects.list(membership=True):
        yield (project.name, {
            'url': project.ssh_url_to_repo,
            'desc': project.attributes['description']
        })


@needs_gitlab
def fork(projectId):
    project = gitlab.projects.get(projectId)

    forked_project = project.forks.create({})

    return forked_project.ssh_url_to_repo
