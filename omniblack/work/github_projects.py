from functools import wraps
from github import Github
from github.GithubException import UnknownObjectException
from .config import config


github = None


def needs_github(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        global github

        keys = config.keys
        if 'Github' not in keys:
            raise KeyError('Github authtoken is not in keys file')

        if github is None:
            github = Github(keys.Github)

        return func(*args, **kwargs)

    return wrapper


@needs_github
def projects():
    for repo in github.get_user().get_repos():
        yield repo.name, {'url': repo.ssh_url, 'desc': repo.description}


@needs_github
def fork(repoId):
    repo = github.get_repo(repoId)

    forked_repo = repo.create_fork()

    return forked_repo.git_url
