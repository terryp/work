from argparse import ArgumentParser
from importlib.resources import path
from contextlib import contextmanager
from pathlib import Path
from itertools import chain


@contextmanager
def get_file(file_name):
    spec_path = Path(file_name)

    if spec_path.parent != Path('.'):
        subpackage = str(spec_path.parent).split('/')
    else:
        subpackage = tuple()

    package_parts = __package__.split('.')
    package = '.'.join(chain(package_parts, subpackage))

    with path(package, spec_path.name) as file_path:
        yield file_path


def get_dir(dir_path):
    import_path = Path(dir_path)/'__init__.py'
    with get_file(import_path) as path:
        return path.parent


def main():
    parser = ArgumentParser(prog='wrk-file')
    parser.add_argument('file')

    args = parser.parse_args()

    with get_file(args.file) as file_path:
        print(str(file_path))


def dir():
    parser = ArgumentParser(prog='wrk-dir')
    parser.add_argument('dir')

    args = parser.parse_args()

    print(str(get_dir(args.dir)))
