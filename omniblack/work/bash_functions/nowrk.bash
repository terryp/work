nowrk () {
    if [[ -n "${WRK_ENV}" ]]
    then
        if [[ -f "${SRC}/dev-tools/nowrk.ext" ]]
        then
            source "${SRC}/dev-tools/nowrk.ext";
        fi


        path_del "${SRC}/dev-tools/bin";

        if [[ -n "${OLD_LOC}" ]]
        then
            cd "${OLD_LOC}" || exit $?;
        fi

        wrk_reset_variables
    fi
}

export -f nowrk
