wrk () {
    if [[ -n "${WRK_ENV}" ]]
    then
        nowrk
    fi

    local env_name="$1"

    local wrk_path
    IFS=: read -a wrk_path <<< "${WRK_PATH}"
    for prefix in "${wrk_path[@]}"
    do
        if [[ -e "${prefix}/${env_name}/.git" ]]
        then

            local cwd="${PWD}"
            cd "${prefix}/${env_name}" || exit $?;

            wrk_override_variable SRC "${prefix}/${env_name}"

            wrk_override_variable OLD_LOC "${cwd}"

            if [[ -f "${SRC}/dev-tools/wrk.ext" ]]
            then
                source "${SRC}/dev-tools/wrk.ext";
            fi
            path_prepend "${SRC}/dev-tools/bin";

            wrk_override_variable WRK_ENV "${env_name}"
            return 0
        fi
    done

    echo "${env_name} not found in WRK_PATH"
    return 1
}

export -f wrk
