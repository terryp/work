#! /usr/bin/sh

wrk_override_variable() {
    var_name="${1}"
    new_value="${2}"
    if [ -z "${var_name}" ]
    then
        echo "A variable name must be provided as the first parameter" 1>&2
        return 1
    fi

    eval "var_value=\"\${${var_name}}\""

    for overriden_var in ${_wrk_overriden_vars}
    do
        pattern_name="${var_name}"
        if [[ "${overriden_var}" = ${pattern_name}* ]]
        then
            already_overriden="true"
            break
        fi
    done

    if [ -z "${already_overriden}" ]
    then
        if [ -z "${_wrk_overriden_vars}" ]
        then
            _wrk_overriden_vars="${var_name}=${var_value}"
        else
            _wrk_overriden_vars="${_wrk_overriden_vars} ${var_name}=${var_value}"
        fi

        export _wrk_overriden_vars
    fi



    eval "export ${var_name}='${new_value}'"
}

wrk_reset_variables() {
    for overriden_var in ${_wrk_overriden_vars}
    do
        var_name="$(cut -d= -f1 <<<"${overriden_var}")"
        old_value="$(cut -d= -f2- <<<"${overriden_var}")"

        if [ -n "${old_value}" ]
        then
            eval "export ${var_name}='${old_value}'"
        else
            eval "unset ${var_name}"
            eval "export ${var_name}"
        fi
    done

    unset _wrk_overriden_vars
}
