#! /usr/bin/sh

hook_name="$(basename ${0})"

dir="${SRC}/dev-tools/git_hooks/${hook_name}"

if [ ! -d "${dir}" ]
then
    exit 0
fi

for file in "${dir}"/*
do
    if [ ! -f "${file}" ]
    then
        # dir is empty so we got the expansion of "${dir}"/* bask
        exit 0
    fi

    filename="$(basename "${file}")"
    echo "Running ${hook_name} hook ${filename}"
    exec < /dev/tty
    "${file}"
    code=$?

    if [ $code != 0 ]
    then
        echo "${filename} failed with code ${code}" 1>&2
        exit ${code}
    fi
done
