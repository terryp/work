from urllib.parse import urlparse
from functools import partial
from sys import stderr, exit
from os import EX_UNAVAILABLE

printerr = partial(print, file=stderr)


fork_funcs = {}
try:
    from .gitlab_projects import fork as gl_fork
except ImportError:
    pass
else:
    fork_funcs['gitlab'] = gl_fork

try:
    from .github_projects import fork as gh_fork
except ImportError:
    pass
else:
    fork_funcs['github'] = gh_fork

def fork(project_url):
    url_parsed = urlparse(project_url)

    path = url_parsed.path.removeprefix('/')
    org, repo_name = path.split('/', maxsplit=1)
    hostname = url_parsed.hostname.lower().removesuffix('.com')

    if hostname not in fork_funcs:
        printerr(f'No library is installed for forking {hostname} projects')
        exit(EX_UNAVAILABLE)

    fork_func = fork_funcs[hostname]

    return (repo_name, fork_func(path))
