from argparse import (
    ArgumentError,
    SUPPRESS,
    _UNRECOGNIZED_ARGS_ATTR,
    _SubParsersAction,
)


default = object()


class DefaultParserAction(_SubParsersAction):
    def set_default_parser(self, name, **kwargs):
        # set prog from the existing prefix
        if kwargs.get('prog') is None:
            kwargs['prog'] = '%s %s' % (self._prog_prefix, name)

        # create a pseudo-action to hold the choice help
        if 'help' in kwargs:
            help = kwargs.pop('help')
            choice_action = self._ChoicesPseudoAction(name, tuple(), help)
            self._choices_actions.append(choice_action)

        parser_name  = f'[{name}]'

        self._name_parser_map[parser_name] = default

        # create the parser and add it to the map
        parser = self._parser_class(**kwargs)
        self.default_parser = parser

        return parser

    def __call__(self, parser, namespace, values, option_string=None):
        parser_name = values[0]
        if parser_name in self._name_parser_map:
            parser = self._name_parser_map[parser_name]
            if parser is default:
                parser = self.default_parser
                return self._call_default_parser(parser, namespace, values)

        return super().__call__(parser, namespace, values, option_string)

    def _call_default_parser(self, parser, namespace, values):
        parser = self.default_parser

        # parse all the remaining options into the namespace
        # store any unrecognized options on the object, so that the top
        # level parser can decide what to do with them

        # In case this subparser defines new defaults, we parse them
        # in a new namespace object and then update the original
        # namespace for the relevant parts.
        subnamespace, arg_strings = parser.parse_known_args(values, None)
        for key, value in vars(subnamespace).items():
            setattr(namespace, key, value)

        if arg_strings:
            vars(namespace).setdefault(_UNRECOGNIZED_ARGS_ATTR, [])
            getattr(namespace, _UNRECOGNIZED_ARGS_ATTR).extend(arg_strings)





