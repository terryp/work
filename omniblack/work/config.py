#! /usr/bin/env python3
from concurrent.futures import ThreadPoolExecutor
from os.path import expandvars
from pathlib import Path
from os import environ
from functools import cache
from contextlib import suppress

from pkg_resources import resource_stream
from ruamel.yaml import YAML
from requests import get

from omniblack.path import ProgramFiles

from .env_reader import env_vars

name = 'work'
files = ProgramFiles(name)


yaml = YAML(typ='safe', pure=True)

pool = ThreadPoolExecutor(max_workers=5)


def get_default_config():
    return resource_stream('omniblack.work', 'work/default_config.yaml')


def get_remotes(remotes_url):
    with get(remotes_url, stream=True) as response:
        response.raw.decode_content = True
        return yaml.load(response.raw)


def expand(path):
    return Path(expandvars(path)).expanduser()



class WrkPath:
    def __init__(self, entries):
        self.entries = tuple(entries)
        self.write = self.entries[0]

    def __iter__(self):
        for entry in self.entries:
            with suppress(FileNotFoundError):
                for item in entry.iterdir():
                    if item.joinpath('.git').exists():
                        yield item

    @cache
    def get(self, env_name):
        for prefix in self.entries:
            if prefix.joinpath(env_name, '.git').exists():
                return prefix/env_name
        else:
            raise FileNotFoundError(f'{env_name} was not found along WRK_PATH')

def get_path() -> WrkPath:
    if environ.get('WRK_PATH'):
        return WrkPath(
            expand(entry)
            for entry in environ['WRK_PATH'].split(':')
        )
    else:
        return WrkPath((Path.home()/'programming_projects', ))

class Config:
    def __init__(self, values):
        keys = expand(values['keys'])
        del values['keys']
        self.__keys_future = pool.submit(env_vars, keys)

        remotes = expand(values['remotes'])
        del values['remotes']
        self.__remotes_future = pool.submit(get_remotes, remotes)

        self.path = get_path()

    @property
    def remotes(self):
        remotes = self.__remotes_future.result()
        return remotes

    @property
    def keys(self):
        keys = self.__keys_future.result()
        return keys

def copy_default_config(self):
    try:
        default_config_file = get_default_config()
        config_file = files.get_config_file(name)
        config_file.config_file_sync(default_config_file, mode='wb')
        return True
    except FileExistsError:
        return False


def init_config():
    try:
        file = files.get_config_file(name)
        return Config(file.get_data_sync())
    except FileNotFoundError:
        copy_default_config()
        return init_config()
    except PermissionError:
        print(f'This program needs premission to write to {file.path}')
        raise SystemExit


config = init_config()
