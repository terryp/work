#! /usr/bin/zsh

# Functions for bash to help with path manipulation



for file in "$(wrk_dir shell_functions)"/* "$(wrk_dir bash_functions)"/*
do
    if [[ "${file}" != *.py && -f "${file}" ]]
    then
        source "${file}"
    fi
done

eval "$(register-python-argcomplete mkwrk)"
eval "$(register-python-argcomplete rmwrk)"
