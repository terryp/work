#! /usr/bin/zsh

# Functions for bash to help with path manipulation

compdef "_files -W wrk_path -/" wrk

typeset -T WRK_PATH wrk_path
typeset -U wrk_path

for file in "$(wrk_dir zsh_functions)"/*
do
    if [[ "$file" != *.py && -f "$file" ]]
    then
        autoload -Uz "${file}"
    fi
done

for file in "$(wrk_dir shell_functions)"/*
do
    if [[ "$file" != *.py && -f "$file" ]]
    then
        source "${file}"
    fi
done

eval "$(register-python-argcomplete mkwrk)"
eval "$(register-python-argcomplete rmwrk)"
