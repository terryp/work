Omniblack work
**************

Omniblack work offers an development enviroment manager.
The primary entry point of this package is the shell function :code:`wrk`.

This function can be accesed adding one of these lines to your shell's
configuration file.

.. code-block:: shell

    # In ~/.bashrc for bash
    source "$(wrk_file env_files/wrk.bash)"

   # In ~/.zshrc for zsh
   source "$(wrk_file env_files/wrk.zsh)"


