.PHONY: build

build := ./.venv/bin/python3 -m build
pip := ./.venv/bin/python3 -m pip

build:
	${build}

dist:
	${poetry} dist
